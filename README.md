# HaProxy MySQL Load Balance

Jalankan 
```
docker-compose up -d --build
```

Haproxy Statistic
http://ip-server:9000/stats
- `user_default :   admin`
- `pass_default :   password`

konfigurasi database master-slave, user dan password terdapat pada file `haproxy.cfg`